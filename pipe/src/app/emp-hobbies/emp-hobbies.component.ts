import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-emp-hobbies',
  templateUrl: './emp-hobbies.component.html',
  styleUrls: ['./emp-hobbies.component.css']
})
export class EmpHobbiesComponent implements OnInit {
  hobbies: any[] = [
    { name: 'jyotshna', hobbie: 'dancing' },
    { name: 'sneha', hobbie: 'singing' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
