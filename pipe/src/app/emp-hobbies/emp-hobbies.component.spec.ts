import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpHobbiesComponent } from './emp-hobbies.component';

describe('EmpHobbiesComponent', () => {
  let component: EmpHobbiesComponent;
  let fixture: ComponentFixture<EmpHobbiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpHobbiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpHobbiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
