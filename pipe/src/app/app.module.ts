import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MypipePipe } from './mypipe.pipe';
import { EmpHobbiesComponent } from './emp-hobbies/emp-hobbies.component';

@NgModule({
  declarations: [
    AppComponent,
    MypipePipe,
    EmpHobbiesComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
