import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { DataService } from './data.service';
import { EmphobbiesComponent } from './emphobbies/emphobbies.component';

@NgModule({
  declarations: [
    AppComponent,
    EmphobbiesComponent
  ],
  imports: [
    BrowserModule,FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
