import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  hobbies = [
    'dancing',
    'singing',
    'internet'
  ];

  constructor() { }

  servicemethod() {
    return 'Its just a simple service method';
  }
}
