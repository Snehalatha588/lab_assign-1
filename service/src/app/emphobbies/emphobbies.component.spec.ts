import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmphobbiesComponent } from './emphobbies.component';

describe('EmphobbiesComponent', () => {
  let component: EmphobbiesComponent;
  let fixture: ComponentFixture<EmphobbiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmphobbiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmphobbiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
