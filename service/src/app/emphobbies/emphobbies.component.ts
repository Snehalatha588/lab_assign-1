import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service'

@Component({
  selector: 'app-emphobbies',
  templateUrl: './emphobbies.component.html',
  styleUrls: ['./emphobbies.component.css']
})
export class EmphobbiesComponent implements OnInit {

  hobbies = [];

  constructor(private _ds: DataService) { }

  ngOnInit() {
    this.hobbies=this._ds.hobbies;
  }

}
